﻿using System;
using UnityEngine;

[Serializable]
public class TankSave : Save {

    // data is a  serializable class within the tanks save script
    public Data data;
    // tank class variable named tank
    private Tank tank;
    // string variable that will hold json translated info
    private string jsonString;

    // public class named data
    // class is filled with the variables that are being saved by the gameSaveManager.
    [Serializable]
    public class Data : BaseData {
        // this will hold the tanks positoin in the world upon a save
        public Vector3 position;
        // this will hold the tanks rotation in the world upon a save
        public Vector3 eulerAngles;
        // this will hold the destination the tank was going to upon a save
        public Vector3 destination;
    }

    void Awake() {
        // find the tank class component
        tank = GetComponent<Tank>();
        // data makes a reference to the Data class within the script
        data = new Data();
    }

    // this takes all the data and saves it as a json file
    public override string Serialize() {
        data.prefabName = prefabName;
        data.position = tank.transform.position;
        data.eulerAngles = tank.transform.eulerAngles;
        data.destination = tank.destination;
        jsonString = JsonUtility.ToJson(data);
        return (jsonString);
    }

    // coverts a saved json lines into code usable by unity
    public override void Deserialize(string jsonData) {
        JsonUtility.FromJsonOverwrite(jsonData, data);
        tank.transform.position = data.position;
        tank.transform.eulerAngles = data.eulerAngles;
        tank.destination = data.destination;
        tank.name = "Tank";
    }
}