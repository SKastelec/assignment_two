﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System;
using System.IO;

[Serializable]
public class GameSaveManager : MonoBehaviour {
    //set the gameDataFilename to the json save file
    public string gameDataFilename = "game-save.json";
    // private string named gameDataPath
    //this will be used to hold the destination route to access the Json file
    private string gameDataFilePath;
    // public list of savable objects
    public List<string> saveItems;
    // a bool to see if the game is being played for the first time, a load turns this to false
    public bool firstPlay = true;

    // Singleton pattern from:
    // http://clearcutgames.net/home/?p=437

    // Static singleton property
    public static GameSaveManager Instance { get; private set; }



    //  to account for deprecated OnLevelWasLoaded() - http://answers.unity3d.com/questions/1174255/since-onlevelwasloaded-is-deprecated-in-540b15-wha.html
    void OnEnable() {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable() {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }


    void Awake() {
        // First we check if there are any other instances conflicting
        if (Instance != null && Instance != this) {
            // If that is the case, we destroy other instances
            Destroy(gameObject);
            return;
        }


        // Here we save our singleton instance
        Instance = this;

        // Furthermore we make sure that we don't destroy between scenes (this is optional)
        DontDestroyOnLoad(gameObject);

        gameDataFilePath = Application.persistentDataPath + "/" + gameDataFilename;
        saveItems = new List<string>();
    }


    public void AddObject(string item) {
        saveItems.Add(item);
    }

    // this is run when i hit save
    public void Save() {
        //clear saveItems list
        saveItems.Clear();

        // create array of Save that finds any object inheriting from Save and puts it into the array
        Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
        //for each item in the array, run the serialize of each object storing the string given to saveItems
        foreach (Save saveableObject in saveableObjects) {
            saveItems.Add(saveableObject.Serialize());
        }

        // use the StreamWritter to write over the game-save.json
        using (StreamWriter gameDataFileStream = new StreamWriter(gameDataFilePath)) {
            // for each string in save items,
            foreach (string item in saveItems) {
                // write to the json file the string in saveItems 
                gameDataFileStream.WriteLine(item);
            }
        }
    }

    //this is run when i hit load
    public void Load() {
        //when i load a level, first play is set to false
        firstPlay = false;
        // clear the saveItems list
        saveItems.Clear();
        //reload the current scene
        //because load scene requires a build index, SceneManager.GetActiveScene().buildIndex gets the index of the current scene thats playing
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }






    //when the scene is loaded
    private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        // if the level is loaded once (when player first start level)
        // Stop, and do not do the next steps
        if (firstPlay) return;

        //at this point, the player has reloaded the game for a second time
        //run loadSaveGameDataFunction
        LoadSaveGameData();
        //run DestroyAllSaveableObjectsInScene
        DestroyAllSaveableObjectsInScene();
        // run CreateGameObjects
        CreateGameObjects();
    }


    void LoadSaveGameData() {
        // use streamReader to read the game-save.json file
        using (StreamReader gameDataFileStream = new StreamReader(gameDataFilePath)) {
            // read but dont do anything until you reach the end 
            while (gameDataFileStream.Peek() >= 0) {
                // take a line and trim any space
                string line = gameDataFileStream.ReadLine().Trim();
                //if there is a line to add
                if (line.Length > 0) {
                    //add the line to saveItems
                    saveItems.Add(line);
                }
            }
        }
    }
    
    //find all the savable objects in the scene, and destroy them
    void DestroyAllSaveableObjectsInScene() {
        //find all objects that inherit for Save into an array
        Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
        // for each item in the array
        foreach (Save saveableObject in saveableObjects) {
            //destroy every gameobject in saveable objects
            Destroy(saveableObject.gameObject);
        }
    }

    //create new objects in the scene 
    void CreateGameObjects() {
        // for every string in the saveItems array
        foreach (string saveItem in saveItems) {

            string pattern = @"""prefabName"":""";
            int patternIndex = saveItem.IndexOf(pattern);
            int valueStartIndex = saveItem.IndexOf('"', patternIndex + pattern.Length - 1) + 1;
            int valueEndIndex = saveItem.IndexOf('"', valueStartIndex);
            string prefabName = saveItem.Substring(valueStartIndex, valueEndIndex - valueStartIndex);

            //create a new gameobject
            GameObject item = Instantiate(Resources.Load(prefabName)) as GameObject;
            //for the new gameobject, sendMessage to the deserialize method
            item.SendMessage("Deserialize", saveItem);
        }
    }
}
