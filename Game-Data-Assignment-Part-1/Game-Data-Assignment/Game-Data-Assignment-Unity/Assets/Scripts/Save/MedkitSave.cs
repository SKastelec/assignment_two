﻿using System;
using UnityEngine;

[Serializable]
public class MedkitSave : Save
{
    // data is a  serializable class within the tanks save script
    public Data data;
    // tank class variable named tank
    private Medkit medkit;
    // string variable that will hold json translated info
    private string jsonString;

    // public class named data
    // class is filled with the variables that are being saved by the gameSaveManager.
    [Serializable]
    public class Data : BaseData
    {
        // this will hold the tanks positoin in the world upon a save
        public Vector3 position;
    }

    void Awake()
    {
        medkit = GetComponent<Medkit>();

        data = new Data();
        
    }

    public override string Serialize()
    {
        data.prefabName = prefabName;
        data.position = medkit.transform.position;
        jsonString = JsonUtility.ToJson(data);
        return (jsonString);

    }


    public override void Deserialize(string jsonData)
    {
        JsonUtility.FromJsonOverwrite(jsonData, data);
        medkit.transform.position = data.position;
        medkit.name = "Health";
    }
}
