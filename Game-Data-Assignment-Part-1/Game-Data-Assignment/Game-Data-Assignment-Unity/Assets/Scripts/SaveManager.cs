﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
    public GameObject tankObject;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            SaveGame();
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            LoadGame();
        }
    }


    void SaveGame()
    {
        PlayerPrefs.SetFloat("TankPositionX", tankObject.transform.position.x);
        PlayerPrefs.SetFloat("TankPositionY", tankObject.transform.position.y);
        PlayerPrefs.SetFloat("TankPositionZ", tankObject.transform.position.z);

        Debug.Log("saved tank position");
    }

    void LoadGame()
    {
        if (PlayerPrefs.HasKey("TankPositionX"))
        {
            float tempX = PlayerPrefs.GetFloat("TankPositionX");
            float tempY = PlayerPrefs.GetFloat("TankPositionY");
            float tempZ = PlayerPrefs.GetFloat("TankPositionZ");

            tankObject.transform.position = new Vector3(tempX, tempY, tempZ);

            Debug.Log("loaded tank Position");

        }
    }
}
